# Le smart-contract du Casino #
```
#!Solidity

pragma solidity ^0.4.6;
   
     contract Roulette {
       
       uint public lastRoundTimestamp;
       uint public nextRoundTimestamp;
       uint _interval;
       address _creator;
       
       enum BetType { Single, Odd, Even }
       struct Bet {
       	BetType betType;
       	address player;
       	uint number;
       	uint value;
       }
   
       Bet[] public bets;
       
       event Finished(uint number, uint nextRoundTimestamp);
       
       function Roulette(uint interval) payable {
       	_interval = interval;
       	_creator = msg.sender;
       	nextRoundTimestamp = now + _interval;
       }
  
     modifier transactionMustContainEther() {
  	    if (msg.value == 0)  {
              throw;
         }
  	    _;
     }
     
     modifier bankMustBeAbleToPayForBetType(BetType betType) {
     	uint necessaryBalance = 0;
     	for (uint i = 0; i < bets.length; i++) {
     		necessaryBalance += getPayoutForType(bets[i].betType) * bets[i].value;
     	}
     	necessaryBalance += getPayoutForType(betType) * msg.value;
     	if (necessaryBalance > this.balance) throw;
     	_;
     }
   
       function betSingle(uint number) payable transactionMustContainEther() bankMustBeAbleToPayForBetType(BetType.Single) {
       	if (number > 36) throw;
       	bets.push(Bet({
       		betType: BetType.Single,
       		player: msg.sender,
       		number: number,
       		value: msg.value
       	}));
       }
       
       function betEven() public payable transactionMustContainEther() bankMustBeAbleToPayForBetType(BetType.Even) {
       	bets.push(Bet({
       		betType: BetType.Even,
       		player: msg.sender,
       		number: 0,
       		value: msg.value
       	}));
       }
   
       function betOdd() public payable transactionMustContainEther() bankMustBeAbleToPayForBetType(BetType.Odd) {
       	bets.push(Bet({
       		betType: BetType.Odd,
       		player: msg.sender,
       		number: 0,
       		value: msg.value
       	}));
       }
       
       function getBetsCountAndValue() public constant returns(uint, uint) {
       	uint value = 0;
       	for (uint i = 0; i < bets.length; i++) {
       		value += bets[i].value;
       	}
       	return (bets.length, value);
       }
       
       function launch() public {
       	if (now < nextRoundTimestamp) throw;
       
       	uint number = uint(block.blockhash(block.number - 1)) % 37;
       	
       	for (uint i = 0; i < bets.length; i++) {
       		bool won = false;
       		uint payout = 0;
       		if (bets[i].betType == BetType.Single) {
       			if (bets[i].number == number) {
       				won = true;
       			}
       		} else if (bets[i].betType == BetType.Even) {
       			if (number > 0 && number % 2 == 0) {
       				won = true;
       			}
       		} else if (bets[i].betType == BetType.Odd) {
       			if (number > 0 && number % 2 == 1) {
       				won = true;
       			}
       		}
                   if (won) {
   			if(!bets[i].player.send(bets[i].value * getPayoutForType(bets[i].betType))) throw;
   		}
       	}
       
       	uint thisRoundTimestamp = nextRoundTimestamp;
       	nextRoundTimestamp = thisRoundTimestamp + _interval;
       	lastRoundTimestamp = thisRoundTimestamp;
       
       	bets.length = 0;
       
       	Finished(number, nextRoundTimestamp);
       }
       
       function getPayoutForType(BetType betType) constant returns(uint) {
       	if (betType == BetType.Single) return 35;
       	if (betType == BetType.Even || betType == BetType.Odd) return 2;
       	return 0;
       }
   }
```

# Le smart contract test Oracle #

```
#!solidity

pragma solidity ^0.4.0;
import "github.com/oraclize/ethereum-api/oraclizeAPI.sol";

contract YoutubeViews is usingOraclize {
    
    string public viewsCount;
    
    event newOraclizeQuery(string description);
    event newYoutubeViewsCount(string views);

    function YoutubeViews() payable {
        update();
    }
    
    function __callback(bytes32 myid, string result) {
        if (msg.sender != oraclize_cbAddress()) throw;
        viewsCount = result;
        newYoutubeViewsCount(viewsCount);
        // do something with viewsCount. like tipping the author if viewsCount > X?
    }
    
    function update() payable {
        newOraclizeQuery("Oraclize query was sent, standing by for the answer..");
        oraclize_query('URL', 'html(https://www.youtube.com/watch?v=9bZkp7q19f0).xpath(//*[contains(@class, "watch-view-count")]/text())');
    }
    
} 
```