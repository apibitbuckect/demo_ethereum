var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	var comptes = [];
	for (var i = 0 ; i < web3.eth.accounts.length ; i++) {
		comptes.push({addr: web3.eth.accounts[i], sold: web3.fromWei(web3.eth.getBalance(web3.eth.accounts[i]), "ether").toNumber()});
	}
  	res.render('index', { title: 'Accueil', comptes: comptes });
});

router.post('/transfert', function(req, res) {
	/*var amount = document.getElementById("amount");
	var from = document.getElementById("from");
	var to = document.getElementById("to");*/
	console.log("Transaction...")
	console.log("from : " + req.body.from);
	console.log("to : " +req.body.to);
	console.log("amount : " + req.body.amount);
	console.log(web3.eth.sendTransaction({from: req.body.from, to: req.body.to, value: web3.toWei(req.body.amount, "ether")}));
	// console.log(from + '->' + to + ' : ' + amount)

	res.redirect('/');
	return ;
});

module.exports = router;
